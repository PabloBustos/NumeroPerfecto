package cl.ubb.NumeroPerfecto;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;



public class NumeroPerfectoTest {

	@Test
	public void Ingresar6RetornarVerdadero() {
		/*arrange*/
		NumeroPerfecto np = new NumeroPerfecto();
		boolean Resultado;
		/*act*/
		Resultado=np.DeterminarNumeroPerfecto(6);
		/* assert */
		assertThat(Resultado,is(true));
	}
	
	@Test
	public void Ingresar28RetornarVerdadero() {
		/*arrange*/
		NumeroPerfecto np = new NumeroPerfecto();
		boolean Resultado;
		/*act*/
		Resultado=np.DeterminarNumeroPerfecto(28);
		/* assert */
		assertThat(Resultado,is(true));
	}
	
	@Test
	public void Ingresar496RetornarVerdadero() {
		/*arrange*/
		NumeroPerfecto np = new NumeroPerfecto();
		boolean Resultado;
		/*act*/
		Resultado=np.DeterminarNumeroPerfecto(496);
		/* assert */
		assertThat(Resultado,is(true));
	}
	
	@Test
	public void Ingresar500RetornarFalso() {
		/*arrange*/
		NumeroPerfecto np = new NumeroPerfecto();
		boolean Resultado;
		/*act*/
		Resultado=np.DeterminarNumeroPerfecto(500);
		/* assert */
		assertThat(Resultado,is(false));
	}

}
